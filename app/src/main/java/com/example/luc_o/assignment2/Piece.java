package com.example.luc_o.assignment2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luc-o on 04/04/2017.
 */

public class Piece {

    public int _x;
    public int _y;
    public int _type;
    public int _opposingType;
    public Piece[][] _board;
    public List<Piece> _close;

    public Piece(int x, int y, int type, Piece[][] board) {
        this._x = x;
        this._y = y;
        this._type = type;
        if (this._type == GlobalHelper.BLUE_PIECE)
            this._opposingType = GlobalHelper.RED_PIECE;
        else if (this._type == GlobalHelper.RED_PIECE)
            this._opposingType = GlobalHelper.BLUE_PIECE;
        this._board = board;
        this._close = new ArrayList<Piece>();
        //this.setClose();
    }

    public void setClose() {
        this._close.clear();
        if (this._y - 1 >= 0 && this._x - 1 >= 0)
            this._close.add(this._board[this._y - 1][this._x - 1]);

        if (this._y - 1 >= 0)
            this._close.add(this._board[this._y - 1][this._x]);

        if (this._y + 1 < 8)
            this._close.add(this._board[this._y + 1][this._x]);

        if (this._x - 1 >= 0)
            this._close.add(this._board[this._y][this._x - 1]);

        if (this._x + 1 < 8)
            this._close.add(this._board[this._y][this._x + 1]);

        if (this._y - 1 >= 0 && this._x + 1 < 8)
            this._close.add(this._board[this._y - 1][this._x + 1]);

        if (this._x + 1 < 8)
            this._close.add(this._board[this._y][this._x + 1]);

        if (this._y + 1 < 8 && this._x + 1 < 8)
            this._close.add(this._board[this._y + 1][this._x + 1]);

    }

}
