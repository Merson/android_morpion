package com.example.luc_o.assignment2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public TextView _player1TextView;
    public TextView _player2TextView;
    public static TextView _playerTurnTextView;
    public Button _resetButton;
    public Button _exitButton;
    public GameLogic _logic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this._player1TextView = (TextView) findViewById(R.id.player1TextView);
        this._player2TextView = (TextView) findViewById(R.id.player2TextView);
        _playerTurnTextView = (TextView) findViewById(R.id.playerTurnTextView);
        this._resetButton = (Button) findViewById(R.id.resetButton);
        this._exitButton = (Button) findViewById(R.id.exitButton);
    }

    public static void changePlayerTurnTextView(int type) {
        if (type == GlobalHelper.BLUE_PIECE) {
            _playerTurnTextView.setText("BLUE TURN");
            _playerTurnTextView.setTextColor(0xFF0000FF);
        } else if (type == GlobalHelper.RED_PIECE) {
            _playerTurnTextView.setText("RED TURN");
            _playerTurnTextView.setTextColor(0xFFFF0000);
        }
    }

    public static void showWinner(int type) {
        if (type == GlobalHelper.BLUE_PIECE) {
            _playerTurnTextView.setText("BLUE PLAYER WON");
            _playerTurnTextView.setTextColor(0xFF0000FF);
        } else if (type == GlobalHelper.RED_PIECE) {
            _playerTurnTextView.setText("RED PLAYER WON");
            _playerTurnTextView.setTextColor(0xFFFF0000);
        }
    }

    public void resetButtonClicked(View v) {
        GameLogic.initBoard();
    }

    public void exitButtonClicked(View v) {
        System.exit(1);
    }
}
