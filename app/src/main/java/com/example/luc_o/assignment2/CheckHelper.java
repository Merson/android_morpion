package com.example.luc_o.assignment2;

/**
 * Created by luc-o on 06/04/2017.
 */

public class CheckHelper {


    public static Boolean checkVerticalUP(Piece p, Piece[][] _board) {
        Boolean opposingTypeFound = false;
        Boolean againSameTypeFound = false;
        int stop = 0;
        for (int y = p._y; y >= 0; y--) {
            if (_board[y][p._x]._type == GlobalHelper.EMPTY_PIECE) {
                break ;
            }
            else if (_board[y][p._x]._type == p._opposingType) {
                opposingTypeFound = true;
            }
            else if (opposingTypeFound && _board[y][p._x]._type == p._type) {
                againSameTypeFound = true;
                stop = y;
            }
        }
        if (againSameTypeFound) {
            for (int y = p._y; y >= stop; y--) {
                _board[y][p._x] = new Piece(p._x, y, p._type, _board);
            }
            return true;
        }
        return false;
    }

    public static Boolean checkVerticalDown(Piece p, Piece[][] _board) {
        Boolean opposingTypeFound = false;
        Boolean againSameTypeFound = false;
        int stop = 0;
        for (int y = p._y; y < 8; y++) {
            if (_board[y][p._x]._type == GlobalHelper.EMPTY_PIECE)
                break ;
            else if (_board[y][p._x]._type == p._opposingType) {
                System.out.println("OPPOSING TYPE");

                opposingTypeFound = true;
            }
            else if (opposingTypeFound && _board[y][p._x]._type == p._type) {
                System.out.println("AGAIN SAME TYPE");

                againSameTypeFound = true;
                stop = y;
            }
        }
        if (againSameTypeFound) {
            for (int y = p._y; y <= stop; y++) {
                _board[y][p._x] = new Piece(p._x, y, p._type, _board);
            }
            return true;
        }
        return false;
    }

    public static Boolean checkHorizontalLeft(Piece p, Piece[][] _board) {
        Boolean opposingTypeFound = false;
        Boolean againSameTypeFound = false;
        int stop = 0;
        for (int x = p._x; x >= 0; x--) {
            if (_board[p._y][x]._type == GlobalHelper.EMPTY_PIECE)
                break ;
            else if (_board[p._y][x]._type == p._opposingType) {
                opposingTypeFound = true;
            }
            else if (opposingTypeFound && _board[p._y][x]._type == p._type) {
                againSameTypeFound = true;
                stop = x;
            }
        }
        if (againSameTypeFound) {
            for (int x = p._x; x >= stop; x--) {
                _board[p._y][x] = new Piece(x, p._y, p._type, _board);
            }
            return true;
        }
        return false;
    }

    public static Boolean checkHorizontalRight(Piece p, Piece[][] _board) {
        Boolean opposingTypeFound = false;
        Boolean againSameTypeFound = false;
        int stop = 0;
        for (int x = p._x; x < 8; x++) {
            if (_board[p._y][x]._type == GlobalHelper.EMPTY_PIECE)
                break ;
            else if (_board[p._y][x]._type == p._opposingType) {
                opposingTypeFound = true;
            }
            else if (opposingTypeFound && _board[p._y][x]._type == p._type) {
                againSameTypeFound = true;
                stop = x;
            }
        }
        if (againSameTypeFound) {
            for (int x = p._x; x <= stop; x++) {
                _board[p._y][x] = new Piece(x, p._y, p._type, _board);
            }
            return true;
        }
        return false;
    }

    public static Boolean checkDiagonalUp(int x, int y, Piece[][] _board) {
        System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        System.out.println("X = " + x);
        System.out.println("Y = " + y);
        return false;
    }

    public static Boolean checkDiagonalDown(Piece p, Piece[][] _board) {
        Boolean opposingTypeFound = false;
        Boolean againSameTypeFound = false;
        int x = p._x;
        int y = p._y;
        if (x < 0 || y < 0)
            return false;
        int stopx = 0;
        int stopy = 0;
        while (p._x < 8 && p._y < 8) {
            if (_board[y][x]._type == GlobalHelper.EMPTY_PIECE)
                break ;
            else if (_board[y][x]._type == p._opposingType) {
                opposingTypeFound = true;
            }
            else if (opposingTypeFound && _board[y][x]._type == p._type) {
                againSameTypeFound = true;
                stopx = x;
                stopy = y;
            }
            p._x++;
            p._y++;
        }
        x = p._x;
        y = p._y;
        if (againSameTypeFound) {
            while (p._x <= stopx && p._y <= stopy) {
                _board[y][x] = new Piece(x, y, p._type, _board);
                p._x++;
                p._y++;
            }
            return true;
        }
        return false;
    }

    public static Boolean checkDiagonalUPRight(Piece p, Piece[][] _board) {
        Boolean opposingTypeFound = false;
        Boolean againSameTypeFound = false;
        int x = p._x;
        int y = p._y;
        int stopx = 0;
        int stopy = 0;
        while (p._x < 8 && p._y >= 0) {
            if (_board[y][x]._type == GlobalHelper.EMPTY_PIECE)
                break ;
            else if (_board[y][x]._type == p._opposingType) {
                opposingTypeFound = true;
            }
            else if (opposingTypeFound && _board[y][x]._type == p._type) {
                againSameTypeFound = true;
                stopx = x;
                stopy = y;
            }
            p._x++;
            p._y--;
        }
        x = p._x;
        y = p._y;
        if (againSameTypeFound) {
            while (p._x <= stopx && p._y >= stopy) {
                _board[y][x] = new Piece(x, y, p._type, _board);
                p._x++;
                p._y--;
            }
            return true;
        }
        return false;
    }

    public static Boolean checkDiagonalDownLeft(Piece p, Piece[][] _board) {
        Boolean opposingTypeFound = false;
        Boolean againSameTypeFound = false;
        int x = p._x;
        int y = p._y;
        int stopx = 0;
        int stopy = 0;
        while (p._x >= 0 && p._y < 8) {
            if (_board[y][x]._type == GlobalHelper.EMPTY_PIECE)
                break ;
            else if (_board[y][x]._type == p._opposingType) {
                opposingTypeFound = true;
            }
            else if (opposingTypeFound && _board[y][x]._type == p._type) {
                againSameTypeFound = true;
                stopx = x;
                stopy = y;
            }
            p._x--;
            p._y++;
        }
        x = p._x;
        y = p._y;
        if (againSameTypeFound) {
            while (p._x >= stopx && p._y <= stopy) {
                _board[y][x] = new Piece(x, y, p._type, _board);
                p._x++;
                p._y--;
            }
            return true;
        }
        return false;
    }
}
