package com.example.luc_o.assignment2;

/**
 * Created by luc-o on 04/04/2017.
 */

public class GlobalHelper {

    public static final int EMPTY_PIECE = 0;
    public static final int BLUE_PIECE = 1;
    public static final int RED_PIECE = -1;

}
