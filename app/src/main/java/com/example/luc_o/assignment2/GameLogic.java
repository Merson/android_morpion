package com.example.luc_o.assignment2;

/**
 * Created by luc-o on 03/04/2017.
 */

public class GameLogic {

    public static int _size;
    public int _current_player;
    public static Piece[][] _board;
    public static GameBoardView _view;
    public static GameLogic _instance;
    public Piece _lastPiece;

    public GameLogic(int size, GameBoardView view) {
        _size = size;
        this._current_player = GlobalHelper.BLUE_PIECE;
        _board = new Piece[size][size];
        _view = view;
        initBoard();
    }

    public static void initBoard() {
        for (int y = 0; y < _size; ++y) {
            for (int x = 0; x < _size; ++x) {
                _board[y][x] = new Piece(x, y, GlobalHelper.EMPTY_PIECE, _board);
            }
        }
        _board[3][3]._type = GlobalHelper.RED_PIECE;
        _board[3][4]._type = GlobalHelper.BLUE_PIECE;
        _board[4][3]._type = GlobalHelper.BLUE_PIECE;
        _board[4][4]._type = GlobalHelper.RED_PIECE;
        _view.invalidate();
    }

    /*public void setBoardClose() {
        for (int y = 0; y < _size; ++y) {
            for (int x = 0; x < _size; ++x) {
                _board[y][x].setClose();
            }
        }
    }*/

    public static GameLogic getInstance(int size, GameBoardView view) {
        if (_instance == null)
            _instance = new GameLogic(size, view);
        return _instance;
    }

    public Boolean checkPlayable() {
        for (int y = 0; y < _size; ++y) {
            for (int x = 0; x < _size; ++x) {
                if (checkCapturable(_board[y][x])) {
                    return true;
                }
            }
        }
        return false;
    }

    public void checkWinner() {
        int bluePoint = 0;
        int redPoint = 0;
        for (int y = 0; y < _size; ++y) {
            for (int x = 0; x < _size; ++x) {
                if (_board[y][x]._type == GlobalHelper.BLUE_PIECE)
                    bluePoint += 1;
                else if (_board[y][x]._type == GlobalHelper.RED_PIECE)
                    redPoint += 1;
                }
        }
        if (bluePoint >= redPoint)
            MainActivity.showWinner(GlobalHelper.BLUE_PIECE);
        else
            MainActivity.showWinner(GlobalHelper.RED_PIECE);
    }

    public Boolean checkCapturable(Piece p) {
        if (CheckHelper.checkVerticalUP(p, _board) ||
                CheckHelper.checkVerticalDown(p, _board) ||
                CheckHelper.checkHorizontalLeft(p, _board) ||
                CheckHelper.checkHorizontalRight(p, _board) ||
                CheckHelper.checkDiagonalUp(p._x, p._y, _board) ||
                CheckHelper.checkDiagonalDown(p, _board) ||
                CheckHelper.checkDiagonalUPRight(p, _board) ||
                CheckHelper.checkDiagonalDownLeft(p, _board)
        ) {
            return true;
        }
        return false;
    }

    public Boolean handleAction(int x, int y) {
        System.out.println("Handle action: x = " + x + " y = " + y);
        if (x > _size || y > _size || _board[y][x]._type != 0) {
             System.out.println("Out of the board");
             return false;
         }
        _board[y][x] = new Piece(x, y, this._current_player, _board);
        if (!this.checkCapturable(_board[y][x])) {
            System.out.println("NO CAPTURE POSSIBLE");
            _board[y][x] = new Piece(x, y, GlobalHelper.EMPTY_PIECE, _board);
            return false;

        }
        this.changeCurrentPlayer();
        MainActivity.changePlayerTurnTextView(this._current_player);
        this._lastPiece = _board[y][x];
        //this.checkPlayable();
        //_board[y][x].setClose();
        /*if (!checkPlayable())
            checkWinner();*/
        return true;
    }

    private void changeCurrentPlayer() {
        if (this._current_player == GlobalHelper.BLUE_PIECE)
            this._current_player = GlobalHelper.RED_PIECE;
        else
            this._current_player = GlobalHelper.BLUE_PIECE;
    }

    public void displayBoardLogic() {
        for (int y = 0; y < _size; ++y) {
            for (int x = 0; x < _size; ++x) {
                System.out.print(_board[y][x]._type + " ");
            }
            System.out.print("\n");
        }
    }
}
