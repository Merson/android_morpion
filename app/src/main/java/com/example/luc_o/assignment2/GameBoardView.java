package com.example.luc_o.assignment2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by luc-o on 03/04/2017.
 */

public class GameBoardView extends View {

    public TextView _playerTurnTextView;

    Context context;
    public int _boardScreenSize;
    public int _BoardDims;
    public int _cellSize;
    public Paint _red;
    public Paint _blue;
    public Paint _yellow;
    public Paint _paint;
    private GameLogic _gl;

    public GameBoardView(Context c, AttributeSet as) {
        super(c, as);
        init(c);
    }

    public GameBoardView(Context c, AttributeSet as, int default_style) {
        super(c, as, default_style);
        init(c);
    }

    private void init(Context c) {
        this.context = c;
        this._boardScreenSize = 0;
        this._BoardDims = 8;
        this._cellSize = this._boardScreenSize / this._BoardDims;

        this._red = new Paint(Paint.ANTI_ALIAS_FLAG);
        this._blue = new Paint(Paint.ANTI_ALIAS_FLAG);
        this._yellow = new Paint(Paint.ANTI_ALIAS_FLAG);
        this._red.setColor(0xFFFF0000);
        this._blue.setColor(0xFF0000FF);
        this._yellow.setColor(0xFFFF00FF);
        this._paint = new Paint();
        this._playerTurnTextView = (TextView)findViewById(R.id.playerTurnTextView);
        System.out.println("PLAYER TURN TEXTVIEW = " + this._playerTurnTextView);
        this.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    int x = (int)(event.getX() * _BoardDims / _boardScreenSize);
                    int y = (int)(event.getY() * _BoardDims / _boardScreenSize);
                    if (x >= _BoardDims || y >= _BoardDims || x < 0 || y < 0) {
                        return false;
                    }
                    Toast.makeText(context, "x = " + x + " y = " + y, Toast.LENGTH_SHORT).show();
                    if (_gl.handleAction(x, y))
                        invalidate();
                    _gl.displayBoardLogic();
                    return true;
                }
                return false;
            }
        });
        this._gl = GameLogic.getInstance(8, this);
    }

    public void refreshBoard(Canvas canvas) {
        for (int y = 0; y < this._gl._size; ++y) {
            for (int x = 0; x < this._gl._size; ++x) {
                if (this._gl._board[y][x]._type == GlobalHelper.BLUE_PIECE) {
                    canvas.drawCircle((x * this._cellSize) + this._cellSize / 2, (y * this._cellSize) + this._cellSize / 2, this._cellSize / 3, this._blue);
                }
                else if (this._gl._board[y][x]._type == GlobalHelper.RED_PIECE) {
                    canvas.drawCircle((x * this._cellSize) + this._cellSize / 2, (y * this._cellSize) + this._cellSize / 2, this._cellSize / 3, this._red);
                }
            }
        }
    }

    private void showPayable(Canvas canvas) {
        if (this._gl._lastPiece == null) {
            return ;
        }
        Piece tmp = this._gl._lastPiece;
        for (int i = 0; i  < tmp._close.size(); ++i) {
                if (tmp._close.get(i) != null)
                    canvas.drawCircle((tmp._close.get(i)._x * this._cellSize) + this._cellSize / 2, (tmp._close.get(i)._y * this._cellSize) + this._cellSize / 2, this._cellSize / 7, this._yellow);
            }
        }

    @Override
    protected void onMeasure(int width, int height){
        int parentWidth = MeasureSpec.getSize(width);
        int parentHeight = MeasureSpec.getSize(height);
        if(parentHeight > parentWidth) {
            this._boardScreenSize = parentWidth;
        }
        else {
            this._boardScreenSize = parentHeight;
        }
        this.setMeasuredDimension(this._boardScreenSize, this._boardScreenSize);
        this._cellSize = this._boardScreenSize / this._BoardDims;
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int i;
        this._paint.setColor(Color.BLACK);
        this._paint.setStrokeWidth(3);
        for(i = 0; i < this._BoardDims; i++) {
            canvas.drawLine(i * this._cellSize, 0, i * this._cellSize, this._boardScreenSize, this._paint);
        }
        canvas.drawLine(this._boardScreenSize, 0, this._boardScreenSize, this._boardScreenSize, this._paint);

        for(i=0; i < this._BoardDims; i++) {
            canvas.drawLine(0, i * this._cellSize, this._boardScreenSize, i * this._cellSize, this._paint);
        }
        canvas.drawLine(0, this._boardScreenSize, this._boardScreenSize, this._boardScreenSize, this._paint);
        this.refreshBoard(canvas);
        showPayable(canvas);
    }


    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }


}
